'use strict'

var authModel = () => {}

authModel.logIn = (email, pass) => {
    var auth = firebase.auth(),
        promise = auth.signInWithEmailAndPassword(email, pass)
    promise
        .then((email, pass) => {
            console.log(pass)
            setCookie(email, pass)
        })

        .catch(e => console.log(e.message))
    auth.onAuthStateChanged(firebaseUser => {
        if(firebaseUser){
            var authContainer = document.getElementsByClassName('auth')[0]
            alert('logged')
            window.location.replace('/')
        }
    })
}
