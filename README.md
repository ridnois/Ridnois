# Ridnois


## El argumento principal para crear ridnois es la necesidad de descentralizar el mercado de valores, su objetivo es crear un sistema de meritocracia para empresas y productores emergentes

#### Para lograr dicho objetivo implementaremos el siguiente algoritmo

###### supongamos que deseamos vender una __`Zapatilla`__ , para hacer esto debemos comprender que `__Zapatilla__` vive dentro del contexto `__Zapato deportivo__` el cual a su vez vive en el contexto ###### `__ropa__`
###### Y el contexto ropa vive en el contexto `__Primeras necesidades__`

#### Entonces, La base de datos de ridnois (en una primera instancia ) luciria de esta manera:

```javascript
    productos : {
        primera_necesidad : {
            ropa : {
                deportiva : {
                    zapatilla : {
                        mi_zapatilla: {
                            //aca una serie de descripciones
                        },
                        otra_zapatilla: {//...}
                    }
                },
                casual : {//...}
            }
        },
        segunda_necesidad : {
            
            //...

        }
    }
```

#### Cuando el usuario o empresa ingresa el producto, está alimentando una base de datos no relacional, en la cual pueden crearse libremente categorias, este es el esqueleto de
#### nuestro sistema de productos

##### A su vez, nuestro objeto en especifico debe ser almacenado como una coleccion en si misma. nuestro producto es un objeto con multiples valores

```javascript
    mi_zapatilla : {
        materiales : [],
        fabricacion: {},
        procedencia: 'China',
        marca : 'Nike'
    }

```
##### Basicamente, el algoritmo recompenza con visibilidad al producto que mas informacion proporciona de si mismo, ya que a traves de esta informacion puede ejecutarse el calculo 
##### para optimizar el sistema, la informacion de este producto ahora esta disponible para compararse.

###### Entonces suponiendo ahora que somos un cliente, al comparar dos productos haremos la comparacion. 

>El cliente analiza que producto posee una mejor calidad, cual esta construido con los mejores materiales, cual tiene mayor visibilidad y mejor marketing,
>mejor relacion precio calidad y mayor consciencia en sus metodos de produccion. como puedes analizar la maquina puede ejercer este proceso de forma mucho mas eficiente


```javascript
    (() => {
        //obtener producto base
        var mi_product = zapatilla,
            calcular = () => {
                for(index in mi_product){
                    var gree_value : () => {
                        //en esta ecuacion consideramos el impacto verde, en base a los indices entregados por el productos 
                        //(La inteligencia artificial analizara si estos son correctos en base a fisica)

                        //insertaremos un algoritmo ridiculamente simple

                        var produc_waste = index.production_material - index.product_material

                        if(product_waste < average_waste){
                            return "Este producto es el mas ecologico dentro de su tipo"
                        } 
                    }
                }
            }
    })()
```

>Ejerciendo esta clase de calculos, de forma automatica y comparandolo con el resto de los productos, podemos analizar los productos mas aventajados segun distintas categorias

Por falta de servidor fps o similar, El desarrollo del algoritmo madre sera postergado hasta nuevo aviso, mientras tanto puede trabajarse en la base de datos y las vistas



Pay system

    -Multiple paid methods
        1) Directly to Ridnois
        2) Person to person
    -Multiple currency
        -Cryptocurrencies
        -Bank transfer
        -Hand to hand

App model
    -Work express as instragram
    -Content Displayed on index through algorithm
    -Addicitive

Algorithm 
    -Self calculated prices:
        "Los precios son calculados en base a algoritmos"
    -High / Low Tendencies:
        "Vendedores pueden predecir el alza y la baja de sus productos"
    -Meritocracy
        "Productos mas verdes, con mejores estandares de produccion son recompenzados con mayor visibilidad, y por lo tanto mayor demanda lo que incrementa su valor neto"
para Mejorar el mundo